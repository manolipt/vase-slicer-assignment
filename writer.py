import math


def write_startup_sequence(gcode, bed_temp, extruder_temp):
    '''
    Writes the startup sequence for the 3D Printer. From Cura's "Start G-Code" for the Ender 3 Pro.
    Function Mapping: file, float, float -> Nothing
        Side Effect: Writes to gcode
        
    Inputs:
        gcode (file) : The GCode file to write the startup sequence to
        bed_temp (float) : The appropriate bed temperature for the print in degrees C
        extruder_temp (float) : The appropriate extruder temperature for the print in degrees C
    Outputs:
        Nothing (Side Effect: writes to gcode)
    '''
    gcode.write('M140 S{} ; Set Bed Temp to desired temp \n'.format(bed_temp) + \
                'M105 ; Check Bed Temp \n' + \
                'M190 S{} ; Set Bed Temp to desired temp and block until it reaches that \n'.format(bed_temp) + \
                'M104 S{} ; Set Extruder Temp to desired temp \n'.format(extruder_temp) + \
                'M105 ; Check Extruder Temp \n' + \
                'M109 S{} ; Set Extruder Temp to desired temp and block until it reaches that \n'.format(extruder_temp) + \
                'M82 ; Absolute extrusion mode \n \n')
        
    gcode.write('G92 E0 ; Reset Extruder \n' + \
                'G28 ; Home all axes \n' + \
                'G1 Z0.3 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed \n' + \
                'G1 X0.1 Y20 Z0.3 F5000.0 ; Move to start position \n' + \
                'G1 X0.1 Y200.0 Z0.3 F1500.0 E15 ; Draw the first line \n' + \
                'G1 X0.4 Y200.0 Z0.3 F5000.0 ; Move to side a little \n' + \
                'G1 X0.4 Y20 Z0.3 F1500.0 E30 ; Draw the second line \n' + \
                'G92 E0 ; Reset Extruder \n' + \
                'G1 Z2.0 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed \n' + \
                'G1 X5 Y20 Z0.3 F5000.0 ; Move over to prevent blob squish \n' + \
                'G0 X110 Y110 Z0.3 ; Move to start location \n \n')
    
    gcode.write('G91 ; Relative Positioning \n' + \
                'M83 ; Relative Extrusion \n')
    
def write_shutdown_sequence(gcode):
    '''
    Writes the shutdown sequence for the 3D Printer. From Cura's "End G-Code" for the Ender 3 Pro.
    Function Mapping: gcode -> Nothing
        Side Effect: Writes to gcode
    Inputs:
        gcode (file) : The GCode file to write the shutdown sequence to
    Outputs:
        Nothing (Side Effect: writes to gcode)
    '''
    gcode.write('G91 ; Relative positioning \n' + \
                'G1 E-2 F2700 ; Retract a bit \n' + \
                'G1 E-2 Z0.2 F2400 ; Retract and raise Z \n' + \
                'G1 X5 Y5 F3000 ; Wipe out \n' + \
                'G1 Z10 ; Raise Z more \n' + \
                'G90 ; Absolute positioning \n \n')
        
    gcode.write('G1 X0 Y220 ; Present print \n' + \
                'M106 S0 ; Turn-off fan \n' + \
                'M104 S0 ; Turn-off hotend \n' + \
                'M140 S0 ; Turn-off bed \n \n')
        
    gcode.write('M84 X Y E ; Disable all steppers but Z \n')

'''
Returns the 2D Distance (xy) between two points. Assumed that the points are coplanar.
Function Mapping: (np.array(3x1 float), np.array(3x1 float)) -> float
    Note: Can also use 2x1 array for this method (xy only) if desired, it just isn't used for that in this program.

Inputs:
    p1, p2 (np.array(3x1 float)) : The points of interest
Returns:
    The xy distance between p1 and p2.
'''
xydist = lambda p1, p2 : math.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)
    

def write_layer(gcode, points, move_speed, extrusion_rate, layer_height):
    '''
    Writes a layer to the given gcode file based on a set of 3D points. 
    Function Mapping: (File, list of np.array(3x1), float, float, float) -> Nothing
        Side Effect: Writes to gcode

    Inputs:
        gcode (File) : The GCode file to write to
        points (np.array(3x1)) : The location of each point (in mm) relative to the center of the print
        move_speed (float) : The speed (in mm/s) at which the end effector should move during printing
        extrusion_rate (float) : The amount the extruder motor should run per unit step of the gantry (in mm/mm)
        layer_height (float) : The height (in mm) of the current print layer
    Returns:
        Nothing (Side Effect: writes to gcode).
    '''
    for i in range(0, len(points)):
        if i == 0:
            gcode.write('G1 X{:.15f} Y{:.15f} F{} ; Move to first point in the layer \n' \
                            .format(points[i][0], points[i][1], move_speed))
        elif i + 1 == len(points):
            gcode.write('G0 X{:.15f} Y{:.15f} E{:.15f} \n' \
                .format(points[i][0]-points[i-1][0], points[i][1]-points[i-1][1], extrusion_rate*xydist(points[i-1], points[i])))
            gcode.write('G1 F300 Z{} ; Move up \n'.format(layer_height))
            gcode.write('G90 ;absolute pos \n' + \
                        'G1 F{} X{} Y{} \n'.format(move_speed, 110, 110) + \
                        'G91 ;relative pos \n')
        else:
            gcode.write('G0 X{:.15f} Y{:.15f} E{:.15f} \n' \
                .format(points[i][0]-points[i-1][0], points[i][1]-points[i-1][1], extrusion_rate*xydist(points[i-1], points[i])))

