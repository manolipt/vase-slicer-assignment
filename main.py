import numpy as np
import matplotlib.pyplot as plt

import generators
import writer

def stl_to_toolpath(stl_filename, layer_height, bed_temp, extruder_temp, move_speed, extrusion_rate, gcode_identifier):
    '''
    Given an STL file and 3D Printer settings/parameters, generate a .gcode file for the toolpath
    required to print the STL file.
    Function Mapping: (str, float, float, float, float, float, str) -> Nothing
        Side Effect: Creates a new file called '[gcode_filename].gcode'.
    
    Inputs:
        stl_filename (str) : The name of the existing STL file to slice
        layer_height (float) : The height of each layer of the print (mm)
        bed_temp (float) : The desired temperature of the print bed (degrees C)
        extruder_temp (float) : The desired temperature of the extruder (degrees C)
        move_speed (float) : The desired move speed of the end effector/extruder (mm/s)
        extrusion_rate (float) : The amount the extrusion motor should run per unit step of the gantry (mm/mm)
        gcode_identifier (str) : The name (excluding '.gcode') of the new gcode file to be generated
    Returns:
        Nothing (Side Effect: creates a new file called '[gcode_filename].gcode').
    '''
    myparser = generators.stlParser(stl_filename)
    triangles = []

    try:
        while True:
            triangles.append(next(myparser))
    except StopIteration:
        mylayers = generators.layerGenerator(triangles, layer_height)

        with open(gcode_identifier + '.gcode', 'x') as gcode:
            writer.write_startup_sequence(gcode, bed_temp, extruder_temp)
            nextlayer = next(mylayers)
            while nextlayer != []:
                writer.write_layer(gcode, nextlayer, move_speed, extrusion_rate, layer_height)
                nextlayer = next(mylayers)
            writer.write_shutdown_sequence(gcode)

def main():
    '''
    Main method for this program - Used to test code
    Current Function Mapping: Nothing -> Nothing
        Requires: 'twist_gear_vase2.stl' in local directory
        Side Effect: Creates 'myvase.gcode' in local directory
    '''
    LAYER_HEIGHT = 0.2
    BED_TEMP = 60
    EXTRUDER_TEMP = 210
    MOVE_SPEED = 1200
    EXTRUSION_RATE = 0.0499

    stl_to_toolpath('twist_gear_vase2.stl', \
                    LAYER_HEIGHT, \
                    BED_TEMP, \
                    EXTRUDER_TEMP, \
                    MOVE_SPEED, \
                    EXTRUSION_RATE, \
                    'myvase')

if __name__ == "__main__":
    main()
