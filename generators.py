import numpy as np

import slicer

def stlParser(stlfile):
    '''
    Generates individual triangles in 3D Euclidean Space (Cartesian Coordinates) by parsing them from an STL file.
    Note that there is an endpoint to this generator, so a try-except statement should be used.
    Function Mapping:
        Initial Call: String -> Generator (Function)
        next() Call: Generator -> R^(3x3)
        
    Inputs:
        stlfile : The file name of the STL file to parse the triangles from.
    Returns:
        Initial Call: A generator to parse the triangles
        next() Call: A list of 3 coordinate tuples (x, y, z) representing the triangle's vertices.
    '''
    with open(stlfile, 'r') as mystl:
        contents = mystl.read().split('\n')
        triang = []
        for line in contents:
            if 'endfacet' in line:
                yield triang
                triang = []
            elif 'vertex' in line:
                start_index = line.index('vertex ') + 7
                coordinates = np.array(list(map(lambda i : float(i), line[start_index:].split(' '))))
                triang.append(coordinates)

def layerGenerator(triangles, slice_height):
    '''
    Given a set of triangles from an STL file, returns the walls of that 3D model layer-by-layer
    as a list of 3D points representing a loop.
    Function Mapping: (list of length 3 lists of np.array(3x1 float), float) -> list of np.array(3x1 float)

    Inputs:
        triangles (list of length 3 lists of np.array(3x1 float)) : The triangles in the STL file
        slice_height (float) : The height of each layer
    Returns:
        Method Call: A Generator Object. No StopIteration condition, 
            but will naturally yield [] when a layer contains no points.
        next() Call: A list of 3D points (np.array(3x1 float)) which form the loop in the current layer.
    '''
    z = 0
    next_layer = []
    while True:
        segments = []
        for t in triangles:
            segments.append(slicer.intersection_triangle_plane(t, z))
        segments = list(filter(lambda s : type(s) is not int, segments))
        next_layer = slicer.make_loop(segments)
        yield next_layer
        z += slice_height
