import numpy as np
import math

def intersection_ray_plane(p0, p1, z):
    '''
    Finds the intersection between a ray (really a segment) and the plane z = (z input)
    using a hand-calculated solution to the algorithm given for the assignment.
    Function Mapping: (np.array(3x1 float), np.array(3x1 float), float) -> np.array(3x1 float)

    Inputs:
        p0, p1 (np.array(3x1 float)) : The two points representing the ray/segment
        z (float) : The elevation of the flat plane with which the intersection is found
    Returns:
        A np.array(3x1 float) representing the 3D point where the ray and plane intersect.
    '''
    n = np.array([0, 0, 1])
    
    if math.isclose(float(np.dot(n, p1 - p0)), 0):
        return -1
    
    r1 = (z - p0[2])/(p1[2] - p0[2])

    out = np.array([r1*(p1[0] - p0[0]) + p0[0], r1*(p1[1] - p0[1]) + p0[1], z])
    
    return out

def value_not_in_between(b, a, c):
    '''
    Returns true if b is NOT in between a and c.
    Function Mapping: (float, float, float) -> boolean

    Inputs:
        b (float) : The number of interest
        a, c (float) : The bounds of interest for 'b'
    Returns:
        False if a < b < c or c < b < a
        True otherwise
    '''
    return not ((a < b and b < c) or (c < b and b < a))

def point_not_in_between(i, t0, t1):
    '''
    Returns true if i is NOT on the line between t0 and t1.
    Function Mapping: (np.array(3x1 float), np.array(3x1 float), np.array(3x1 float)) -> boolean

    Inputs:
        i (float) : The point of interest
        a, c (float) : The bounds of interest for 'b'
    Returns:
        False if i is on the line between t0 and t1
        True otherwise
    '''
    return value_not_in_between(i[0], t0[0], t1[0]) or \
           value_not_in_between(i[1], t0[1], t1[1]) or \
           value_not_in_between(i[2], t0[2], t1[2])

def intersection_triangle_plane(t, z):
    '''
    Finds the intersection of a triangle with the plane z = (z input).
    Function Mapping: (length 3 list of np.array(3x1 float), float)
                        -> length 2 list of np.array(3x1 float)
    
    Inputs:
        t (length 3 list of np.array(3x1 float)) : The triangle of interest, represented by its vertices
        z (float) : The elevation of the flat plane with which the intersection is found
    Returns:
        A length 2 list of np.array(3x1 float) representing the start and end points of the intersection.
    '''
    i1 = intersection_ray_plane(t[0], t[1], z)
    if type(i1) is not int:
        if point_not_in_between(i1, t[0], t[1]):
            i1 = -1
    
    i2 = intersection_ray_plane(t[0], t[2], z)
    if type(i2) is not int:
        if point_not_in_between(i2, t[0], t[2]):
            i2 = -1
    
    i3 = intersection_ray_plane(t[1], t[2], z)
    if type(i3) is not int:
        if point_not_in_between(i3, t[1], t[2]):
            i3 = -1

    intersection_points = list(filter(lambda pt : type(pt) is not int, (i1, i2, i3)))

    if len(intersection_points) < 2 or np.array_equal(intersection_points[0], intersection_points[1]):
        return -1
    
    return intersection_points
    
def make_loop(s):
    '''
    Turns a list of segments into a loop (for 3D purposes, the loop should be closed).
    Function Mapping: list of length 2 lists of np.array(3x1 float) -> list of np.array(3x1 float)

    Inputs:
        s (list of length 2 lists of np.array(3x1 float)) : The segments to make a loop out of
    Returns:
        A list of np.array(3x1 float) representing all of the points in the loop in order.
    '''
    if not s:
        return []
    
    loop = [s[0][0]]
    cur = 0
    missingPt = 1
    prevLen = len(loop)
    
    while len(s) > 0:
        if len(s) == 1:
            loop.append(s[cur][missingPt])
            del s[cur]
            continue
        i = 0
        found = False
        currentLen = len(s)
        while len(s) == currentLen and i < currentLen:
            if i == cur:
                i += 1
                continue
            elif np.allclose(s[cur][missingPt], s[i][0]):
                loop.append(s[cur][missingPt])
                prevLen = len(loop)
                del s[cur]
                if i > cur:
                    cur = i - 1
                else:
                    cur = i
                missingPt = 1
                found = True
            elif np.allclose(s[cur][missingPt], s[i][1]):
                loop.append(s[cur][missingPt])
                prevLen = len(loop)
                del s[cur]
                if i > cur:
                    cur = i - 1
                else:
                    cur = i
                missingPt = 0
                found = True
            i += 1
        if not found and prevLen == len(loop):
            break
    
    return loop
